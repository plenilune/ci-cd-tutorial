package com.plenilune.cicdtutorial.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping
    public String helloWorld() {
        return "Hello, World! Gitlab Springboot CI/CD with k8s";
    }

    @GetMapping("/test")
    public String test() {
        return "Endpoint test";
    }
}
